# app-mu-core ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

app-mu-core is a java library for manufacturing unit application core dependencies to make use of them across the application

## Build

* mvn clean install

## Installation

* mvn clean install

### Requirements

* Java 8
* Maven ~> 3
* Git ~> 2


## Usage

```
 <dependency>
    <groupId>com.app.mu</groupId>
    <artifactId>app-mu-core</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

```

## Development


## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
